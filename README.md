# fms-saas

#### 介绍
面向小微企业财务管理人员，提供全套财务管理功能，渗透小微企业管理各流程，打造全新财务管理模式。

#### 软件技术架构
![输入图片说明](https://images.gitee.com/uploads/images/2021/0726/154022_b603827c_9499198.png "屏幕截图.png")

#### 软件技术栈
![输入图片说明](https://images.gitee.com/uploads/images/2021/0818/154719_5fc27383_9499198.png "屏幕截图.png")

#### 演示地址
财务saas演示环境地址 [http://dcdfms.dev.aiyuncar.com/ ](http://dcdfms.dev.aiyuncar.com/)

账号密码：商户账号 aiyuncar 用户账号及密码 yuangong01/test123456

#### 获取帮助
邮箱：zjt_gz@163.com 

微信

![输入图片说明](https://images.gitee.com/uploads/images/2021/0818/155831_7bc5c4b4_9499198.png "屏幕截图.png")

#### 界面截图

![输入图片说明](https://images.gitee.com/uploads/images/2021/0726/154212_df9cbfb9_9499198.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0726/154241_0c029712_9499198.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0726/154426_a5d214ea_9499198.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0726/154447_b3b01740_9499198.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0726/154528_e11d0451_9499198.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0726/154548_40249787_9499198.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0726/154648_3bfc21e6_9499198.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0726/154718_f2bb0459_9499198.png "屏幕截图.png")